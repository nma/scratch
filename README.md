# README

## Files

## Commands

### Raw gene list to proper gene pairs 
Add the list needed to be transformed, according to its species.
```python
python3 human_data_process.py human1.csv
```

### Gene pairs to NEPDF data
Add the index, corresponding pair file and a number indicating whether it has labels.
```python
python3 NEPDF_human.py index_train.csv train.csv 1
```
 
### To use mouse data
Download its database to `database` folder.
```bash
https://s3.amazonaws.com/mousescexpression/rank_total_gene_rpkm.h5
```


