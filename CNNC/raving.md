1. batch_size = size of samples
2.  For example, gene coexpression analysis is usually performed using Pearson correlation (PC) or mutual information (MI) (6). Functional assignment of genes is often performed using clustering (7) or undirected graphical models including Markov random fields (8), while pathway reconstruction is often based on directed probabilistic graphical models (4). 

**Correlation**

@article{song2012comparison,
  title={Comparison of co-expression measures: mutual information, correlation, and model based indices},
  author={Song, Lin and Langfelder, Peter and Horvath, Steve},
  journal={BMC bioinformatics},
  volume={13},
  number={1},
  pages={328},
  year={2012},
  publisher={Springer}
}

3. Choose one of the human/mouse results: human1 & mouse3 is better

4. problems with current model:
* overfitting
* dropout ratio need to be optimized

`optimized epoch number`, `optimized bins(now 32)`,...


5. fine_tune: 
* reduce layers
* reduce dropout ratio, reserve features
* 
